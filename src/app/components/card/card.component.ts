import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {
  rating = 3.5;
  editableFlags = false;

  flags = [
    '/assets/img/countries/Spain.svg',
    '/assets/img/countries/Turkey.svg',
    '/assets/img/countries/France.svg',
    '/assets/img/countries/United-Kingdom.svg',
    '/assets/img/countries/Ukraine.svg',
    '/assets/img/countries/Russia.svg',
    '/assets/img/countries/Germany.svg',
    '/assets/img/countries/China.svg',
    '/assets/img/countries/Portugal.svg'
  ];

  constructor() { }

  ngOnInit(): void {
  }

  changeEditableFlags(): void {
    this.editableFlags = !this.editableFlags;
  }

  getAngleCenteringFlags(): number {
    const angleStep = 180 / 8;
    return angleStep * (9 - this.flags.length) / 2;
  }

  deleteFlag(flagUrl: string): void {
    this.flags = this.flags.filter(item => (item !== flagUrl));
  }
}
